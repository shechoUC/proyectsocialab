import axios from 'axios'

export default axios.create({
  baseURL: process.env('https://api.magicthegathering.io/v1/')
})