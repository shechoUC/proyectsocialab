import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cards: null,
  },
  mutations: {

    SET_CARDS_DATA(state, CardsData){
      state.cards = CardsData
    }
  },
  actions: {
    async getCards(){
      const { data } = this.$axios.get('/cards')
      this.commit('SET_CARDS_DATA', data)
      console.log(data)
    }
  },
  modules: {
  }
})
